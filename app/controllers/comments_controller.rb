class CommentsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :update, :like, :love]
  before_action :set_post, except: [:like, :love]
  before_action :set_comment, only: [:destroy, :like, :love]

  def create
    @comment = @post.comments.new(comment_params)
    @comment.user = current_user
    @comment.like_number=0
    @comment.save
  end

  def like
    if current_user.voted_up_on? @comment
      @comment.downvote_from current_user
    else
      @comment.liked_by current_user
    end
  end

  def love
    Love.create!(adorable: @comment,user: current_user)
  end
  def destroy
    @comment.destroy
  end

  private

  def set_post
    @post = Post.find(params[:post_id])
  end

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:body)
  end
end