class Love < ActiveRecord::Base
  belongs_to :adorable, polymorphic: true
  belongs_to :user
  belongs_to :receiver, class_name: 'User', foreign_key: 'receiver_id'

  before_validation do
    set_receiver
  end

  scope :checked, -> { where(is_checked: true) }
  scope :unchecked, -> { where(is_checked: false) }

  def set_receiver
    if adorable
      case adorable_type
      when 'Post'
        self.receiver = adorable.user
      when 'Comment'
        self.receiver = adorable.user
      else
        raise 'not supported adorable_type'
      end
    end
  end
end
